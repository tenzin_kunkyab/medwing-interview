MEDWING HOME ASSIGNMENT
===
The assignment task consists of two parts. First to build a simple React application that shows a map and markers on it. A user should be able to view, create, edit, and delete the markers. All changes should be immediately visible on the map. The second part is to actually build the backend API in Ruby ( using Django ) for allowing these CRUD operations for markers

React Application
---
* [x] Using Create React App
* [x] Google API
* [x] Search for `google map for react` npm package
* [x] Edit functionality
    * Two options
        * one where once the user clicks on the `edit` button all other markers on the page is hidden and the particular marker can be dragged and dropped.
            * pros: good UX
            * cons: possibly add atleast couple of hours
        * second where on clicking the edit button, whenever the user clicks on the map, the new position is updated to that card

ReactComponents:
* Break it down into two components, one is the Map, and another is the Marker Details Component


Django Backend API
---
* [x] Create connection between backend and react
* [x] CRUD Operation
    * [x] GET API for Read
    * [x] POST API for Create, and Delete
    * [] POST API for Edit/Update
* [x] Use some DB to store
    * using sqlite3


Implementation
---
* Use Google Geocoding API
* Include Unit tests and Integration feature tests
* Use separate repo for frontend and backend

_for my purpose, I'd put both the frontend and the backend in one repo but under different folder. The reason being it'd be easier for me to check-in code and have historic state of this project at the same time._

Setup
===
* Front-end
    * goto ~/project/frontend
    * npm install
    * npm start
* Backend
    * goto ~/project/backend
    * create a virtualenv
        * virtualenv venv
        * source venv/bin/activate
        * pip install -r requirements.txt
    * cd ~/backend/medwing
    * python manage.py makemigrations
    * python manage.py migrate
    * python manage.py runserver --- should be on port 8000




Known Issues
---
* [] Multiple Edit buttons
* [] API key randomizer -- when one key gets over, use the next key from the list; or get a API with big quota
* [] Missing Tests


