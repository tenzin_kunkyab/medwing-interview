from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from .models import PointOfInterest

import requests 
import json

def get_all_locations(request):
    """ fetches all the entries in PointOfInterests that have 
    `enabledFlag` set to True. """
    data = []
    list_poi = PointOfInterest.objects.filter(enabledFlag=True)
    for item in list_poi:
        data.append({
            'id': item.pk,
            'name': item.name,
            'lat': item.position_lat,
            'lng': item.position_lng
        })
    return JsonResponse({
        'PointOfInterests': data
    })


@csrf_exempt
def add_entry(request):
    """ Again only accept POST calls. Creates a new entry in the 
    PointOfInterest Table with the name/address received from the 
    reverse_goecoder using the lat & lng received. """
    if request.method == 'GET':
        return JsonResponse({
            'response_status': 'failure',
            'message': 'Incorrect call made'
        })
    try:
        loc_lat = request.POST.get('latitude')
        loc_lng = request.POST.get('longitude')
        loc_name = reverse_geocoder(loc_lat, loc_lng)
    except KeyError:
        return JsonResponse({
            'response_status': 'failure',
            'message': 'Invalid information received'
        })
    
    if not loc_name:
        return JsonResponse({
            'response_status': 'failure',
            'message': 'Unable to reverse geocode the location',
            'code': 'GEOCODE_FAIL'
        })
    new_loc_obj = PointOfInterest(name=loc_name, 
                                  position_lat=loc_lat, 
                                  position_lng=loc_lng)
    new_loc_obj.save()
    if new_loc_obj.id:
        return JsonResponse({
            'response_status': 'success',
            'point_of_interest': {
                'id': new_loc_obj.id,
                'name': new_loc_obj.name,
                'lat': new_loc_obj.position_lat,
                'lng': new_loc_obj.position_lng
            }
        })
    return JsonResponse({
        'response_status': 'failure',
        'message': 'Unable to save new position'
    })

@csrf_exempt
def update_entry(request):
    """ Only accepts GET calls. Updates the entry in the 
    DB (if found) with the name/address received from the reverse_goecoder 
    using the lat & lng received. """
    if request.method == 'GET':
        return JsonResponse({
            'response_status': 'failure',
            'message': 'Incorrect call made'
        })
    poi_id = request.POST.get('interest_id')
    try:
        interest_obj = PointOfInterest.objects.get(pk=poi_id)
        new_lat = request.POST.get('latitude')
        new_lng = request.POST.get('longitude')
        new_name = reverse_geocoder(new_lat, new_lng)
        if not new_name:
            return JsonResponse({
                'response_status': 'failure',
                'message': 'Unable to reverse geocode the location',
                'code': 'GEOCODE_FAIL'
            })
        interest_obj.name = new_name
        interest_obj.position_lat = new_lat
        interest_obj.position_lng = new_lng
        interest_obj.save()
        return JsonResponse({
            'response_status': 'success',
            'point_of_interest': {
                'id': poi_id,
                'name': new_name,
                'lat': interest_obj.position_lat,
                'lng': interest_obj.position_lng
            }
        })
    except PointOfInterest.DoesNotExist:
        return JsonResponse({
            'response_status': 'failure',
            'message': 'Interest detail not found'
        })

def delete_entry(request):
    """
    Updated the `enabledFlag` to False for the given point_of_interest id
    """
    poi_id = None
    if request.method == 'GET':
        poi_id = request.GET.get('id')
    else:
        poi_id = request.POST.get('id')
    if (not poi_id):
        return JsonResponse({
            'response_status': 'failure',
            'message': 'No ID received'
        })
    try:
        poi_obj = PointOfInterest.objects.get(id=poi_id)
        poi_obj.enabledFlag = False
        poi_obj.save()
        return JsonResponse({
            'response_status': 'success'
        })
    except PointOfInterest.DoesNotExist:
        return JsonReponse({
            'response_status': 'failure',
            'message': 'No Point of Interest Found'
        })
    

def reverse_geocoder(lat, lng):
    """ returns either None or a String. String can be the 
    address/name associated with the lat&lng"""
    # not a fan of adding such keys in the source code
    # I'd optimally prefer if these keys were either picked up
    # from the production server something in the lane of 
    # _APIKEY_ = os.getenv('GEOCODE_APIKEY', 'development_key')
    _APIKEY_ = "AIzaSyA2wdK8WZZ5v3wsVrBJU5qw7B3QYXpEw9k"
    url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" +\
         lat +","+ lng +"&key=" + _APIKEY_
    response = requests.get(url)
    response = json.loads(response.text)
    if (response["status"] == "OK"):
        if (len(response['results']) > 0):
            # for this purpose, I'm picking up the first formatted_address received from Google's Goecoder
            return response["results"][0]["formatted_address"]
        else:
            return None
    else:
        return None