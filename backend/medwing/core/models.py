
from django.db import models
from django.utils import timezone

class PointOfInterest(models.Model):
    name = models.CharField(max_length=200)
    creation_datetime = models.DateTimeField('creation_datetime')
    update_datetime = models.DateTimeField('update_datetime')
    position_lat = models.DecimalField(
        max_digits=12, decimal_places=9
    )
    position_lng = models.DecimalField(
        max_digits=12, decimal_places=9
    )
    enabledFlag = models.BooleanField('enabledFlag', default=True)
    
    def __str__(self):
        return self.name
    
    def save(self, *args, **kwargs):
        ''' on save, update timestamp '''
        if not self.id:
            self.creation_datetime = timezone.now()
        self.update_datetime = timezone.now()
        return super(PointOfInterest, self).save(*args, **kwargs)