from django.contrib import admin
from django.urls import path

import core.views as core_views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('v1/list', core_views.get_all_locations),
    path('v1/add', core_views.add_entry),
    path('v1/update', core_views.update_entry),
    path('v1/delete', core_views.delete_entry),
]
