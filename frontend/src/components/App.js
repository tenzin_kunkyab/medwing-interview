import React from "react";
import axios from "axios";

import Map from "./Map/Map";
import MarkerList from "./MarkerList/MarkerList";
import "./App.css";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      interestPoints: [],
      backupMarkers: [],
      markers: [],
      failure: false
    };
    this.createNewPointOfInterest = this.createNewPointOfInterest.bind(this);
    this.removeMarker = this.removeMarker.bind(this);
    this.onMarkerSelect = this.onMarkerSelect.bind(this);
    this.updateMarker = this.updateMarker.bind(this);
    this.cancelUpdate = this.cancelUpdate.bind(this);
  }

  dataParser(interests) {
    let markers = [],
      markerList = [];
    interests.forEach(item => {
      markers.push({
        label: item.name,
        uniqueId: item.id,
        isDraggable: false,
        position: {
          lat: parseFloat(item.lat),
          lng: parseFloat(item.lng)
        }
      });
      markerList.push({
        name: item.name,
        id: item.id,
        lat: item.lat,
        lng: item.lng,
        editable: true
      });
    });
    this.setState({
      interestPoints: markerList,
      markers: markers,
      failure: null
    });
  }

  componentDidMount() {
    let cmpt = this;
    axios.get("http://127.0.0.1:8000/v1/list").then(response => {
      if (response.status === 200) {
        if (
          response.data.hasOwnProperty("PointOfInterests") &&
          Array.isArray(response.data.PointOfInterests)
        ) {
          let pointOfInterests = response.data.PointOfInterests;
          if (pointOfInterests.length) {
            cmpt.dataParser(pointOfInterests);
          }
        }
      } else {
        cmpt.setState({
          failure: "fetch"
        });
      }
    });
  }

  async createAPI(lat, lng) {
    let isCreated = false,
      response_data = null;
    const formData = new FormData();
    formData.set("latitude", lat);
    formData.set("longitude", lng);
    const apiCall = await axios({
      method: "POST",
      url: "http://127.0.0.1:8000/v1/add",
      data: formData
    });
    const response = await apiCall;
    if (response.status === 200) {
      if (response.data.response_status === "success") {
        let newPointOfInterest = response.data.point_of_interest;
        isCreated = true;
        response_data = newPointOfInterest;
      } else {
        // handle errors
        if (response.data.code == "GEOCODE_FAIL") {
          // perhaps give the user a form to enter the name, since we already have the lat&lng?
          isCreated = false;
        }
      }
    } else {
      // handle errors
      alert("Error while creating new Interest");
    }
    if (response) {
      return {
        // the purpose of using isCreated is so that I can down the lane add support
        // to extend this enable a user to add their own `label` or `name` to the location
        // there-by bypassing the need for GeoCoding API need. Hasn't been implemented yet
        isCreated,
        response_data
      };
    }
  }

  createNewPointOfInterest(lat, lng) {
    let cmpt = this;
    this.createAPI(lat, lng).then(data => {
      let { isCreated, response_data } = data;
      if (isCreated) {
        let curr_markers = cmpt.state.markers,
          curr_interestPoints = cmpt.state.interestPoints;
        curr_markers.push({
          label: response_data.name,
          uniqueId: response_data.id,
          position: {
            lat: parseFloat(response_data.lat),
            lng: parseFloat(response_data.lng)
          }
        });
        response_data["editable"] = true;
        curr_interestPoints.push(response_data);
        cmpt.setState({
          markers: curr_markers,
          interestPoints: curr_interestPoints,
          failure: null
        });
      } else {
        cmpt.setState(
          {
            failure: "creation"
          },
          function() {
            setTimeout(function() {
              cmpt.setState({
                failure: null
              });
            }, 4000);
          }
        );
      }
    });
  }

  removeMarker(marker_id) {
    // Interest Point is removed
    // This removes the Marker for that InterestPoint
    let new_markers = this.state.markers.filter(
      item => item.uniqueId !== marker_id
    );
    let new_interestPoints = this.state.interestPoints.filter(
      item => item.id !== marker_id
    );
    this.setState({
      markers: new_markers,
      interestPoints: new_interestPoints
    });
  }

  onMarkerSelect(marker_id) {
    // We set only one Marker's `isDraggable` property to `true`
    // so that only one marker is displayed on the Map.
    // We also save the previous `markers` to a new state called `backupMarkers`
    // so that we can restore it.
    let markerToEdit = this.state.markers.filter(
      item => item.uniqueId == marker_id
    );
    markerToEdit[0]["isDraggable"] = true;
    this.setState({
      markers: markerToEdit,
      backupMarkers: this.state.markers
    });
  }

  updateMarker(marker_id, lat, lng) {
    let backupMarkers = this.state.backupMarkers,
      interestPoints = this.state.interestPoints;
    let cmpt = this;
    const formData = new FormData();
    formData.set("latitude", lat);
    formData.set("longitude", lng);
    formData.set("interest_id", marker_id);
    axios({
      method: "POST",
      url: `http://127.0.0.1:8000/v1/update`,
      data: formData
    }).then(response => {
      if (response.status == 200) {
        if (response.data.response_status == "success") {
          // Instead of API listing the entire dataset again
          // we only receive the updated entry
          // Then we just update the entries in the `backupMarkers`
          // and the `InterestPoints` followed by updateding the `state`
          let data = response.data.point_of_interest;
          let newMarkers = backupMarkers.map(item => {
            if (item.uniqueId == marker_id) {
              item.label = data.name;
              item.position = {
                lat: parseFloat(data.lat),
                lng: parseFloat(data.lng)
              };
              item.isDraggable = false;
            }
            return item;
          });
          let updatedInterest = interestPoints.map(item => {
            if (item.id === marker_id) {
              item.name = data.name;
              item.lat = data.lat;
              item.lng = data.lng;
              item.editable = true;
            }
            return item;
          });
          cmpt.setState({
            markers: newMarkers,
            backupMarkers: [],
            interestPoints: updatedInterest,
            failure: null
          });
        }
      } else {
        cmpt.setState(
          {
            markers: backupMarkers,
            interestPoints: interestPoints,
            backupMarkers: [],
            failure: "updation"
          },
          function() {
            setTimeout(function() {
              cmpt.setState({
                failure: null
              });
            }, 4000);
          }
        );
      }
    });
  }

  cancelUpdate(marker_id) {
    let backupMarkers = this.state.backupMarkers,
      interestPoints = this.state.interestPoints;
    let newMarkers = backupMarkers.map(item => {
      if (item.uniqueId == marker_id) {
        item.isDraggable = false;
      }
      return item;
    });
    let updatedInterest = interestPoints.map(item => {
      if (item.id === marker_id) {
        item.editable = true;
      }
      return item;
    });
    this.setState({
      markers: newMarkers,
      backupMarkers: [],
      interests: updatedInterest
    });
  }

  render() {
    let { markers, failure, interestPoints } = this.state;
    let msgDOM = null;
    if (failure) {
      switch (failure) {
        case "fetch": {
          msgDOM = (
            <p className="alert alert-error">
              Unexpected failure while fetching all Markers
            </p>
          );
          break;
        }
        case "creation": {
          msgDOM = (
            <p className="alert alert-error">
              Unexpected failure while creating a new Point of Interest
            </p>
          );
          break;
        }
        case "updation": {
          msgDOM = (
            <p className="alert alert-error">
              Unable to update the particular entry
            </p>
          );
          break;
        }
        default:
          msgDOM = null;
      }
    }
    return (
      <div>
        {msgDOM}
        <Map
          markers={markers}
          updateMarker={this.updateMarker}
          mapClick={this.createNewPointOfInterest}
        />
        <MarkerList
          markerDetails={interestPoints}
          onRemoveMarker={this.removeMarker}
          onMarkerSelect={this.onMarkerSelect}
          cancelUpdate={this.cancelUpdate}
        />
      </div>
    );
  }
}
export default App;
