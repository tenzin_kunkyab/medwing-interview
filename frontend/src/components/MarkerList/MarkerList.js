import React, { Component } from "react";
import axios from "axios";
import "./MarkerList.css";

export default class MarkerList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      markerDetails: []
    };
    this.editMarker = this.editMarker.bind(this);
    this.deleteMarker = this.deleteMarker.bind(this);
    this.cancelUpdate = this.cancelUpdate.bind(this);
  }

  cancelUpdate(markerID) {
    this.props.cancelUpdate(markerID);
  }

  editMarker(markerID) {
    // marking one Marker to be edited
    // replace `edit` with a `cancel` button
    let currMarkers = this.state.markerDetails.map(item => {
      if (item.id === markerID) {
        item.editable = false;
      }
      return item;
    });
    this.setState({
      markerDetails: currMarkers
    });
    this.props.onMarkerSelect(markerID);
  }

  deleteMarker(markerID) {
    let cmpt = this;
    axios
      .get(`http://127.0.0.1:8000/v1/delete?id=${markerID}`)
      .then(response => {
        if (response.status === 200) {
          if (response.data.response_status === "success") {
            // need to remove this entry from the props
            // perhaps move it to a state? to keep things simple
            cmpt.props.onRemoveMarker(markerID);
          }
        }
      });
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      markerDetails: nextProps.markerDetails
    });
  }

  componentWillMount() {
    if (this.props.markerDetails.length) {
      this.setState({
        markerDetails: this.props.markerDetails
      });
    }
  }

  render() {
    let toDisplay = <div>Such Empty, Much Wow!</div>;
    let cmpt = this;
    if (this.state.markerDetails.length) {
      toDisplay = this.state.markerDetails.map((item, key) => (
        <Card
          item={item}
          key={key}
          editable={true}
          editMarker={cmpt.editMarker}
          deleteMarker={cmpt.deleteMarker}
          cancelUpdate={markerID => {
            cmpt.props.cancelUpdate(markerID);
          }}
        />
      ));
    }
    return <div className="marker-list-container">{toDisplay}</div>;
  }
}

class Card extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      editable: true
    };
  }
  editMarker(item_id) {
    this.setState({
      editable: false
    });
    this.props.editMarker(item_id);
  }
  cancelUpdate(item_id) {
    this.setState({
      editable: true
    });
    this.props.cancelUpdate(item_id);
  }
  render() {
    return (
      <div className="marker-card">
        <h2>{this.props.item.name}</h2>
        <address>
          {this.props.item.name}
          <br />
          Latitude: {this.props.item.lat}
          <br />
          Longitude: {this.props.item.lng}
        </address>
        <div className="input-bar">
          {this.state.editable && (
            <input
              type="button"
              onClick={() => {
                this.editMarker(this.props.item.id);
              }}
              value="Edit"
            />
          )}
          {!this.state.editable && (
            <input
              type="button"
              onClick={() => this.cancelUpdate(this.props.item.id)}
              value="Cancel"
            />
          )}
          <input
            type="button"
            onClick={() => this.props.deleteMarker(this.props.item.id)}
            value="Delete"
          />
        </div>
      </div>
    );
  }
}
