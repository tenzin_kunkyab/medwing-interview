import React, { Component } from "react";
import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";

import "./Map.css";

export default class Map extends Component {
  render() {
    return (
      <LoadScript id="script-loader" googleMapsApiKey="">
        <GoogleMap
          id="medwing_marker"
          mapContainerClassName="map-container"
          zoom={13}
          center={{
            lat: 52.52544,
            lng: 13.460518
          }}
          onClick={e => {
            // This is to trigger a new PointOfInterest entry creation
            // When clicked on the map, we hit the `mapClick` props that
            // adds a new entry.
            this.props.mapClick(e.latLng.lat(), e.latLng.lng());
          }}
        >
          {this.props.markers.map((marker, ind) => (
            <Marker
              position={marker.position}
              key={ind}
              title={marker.label}
              uniqueId={marker.uniqueId}
              draggable={marker.isDraggable}
              onDragEnd={ev => {
                // This is enabled or called IFF when the marker.isDraggable props is
                // set to true the event contains the latLng. Hence hitting the
                // updateMarker with `uniqueId`, `lat`, and `lng` to hit the update API
                this.props.updateMarker(
                  marker.uniqueId,
                  ev.latLng.lat(),
                  ev.latLng.lng()
                );
              }}
            />
          ))}
        </GoogleMap>
      </LoadScript>
    );
  }
}
